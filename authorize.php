<?php

if (!empty($_GET['user_hash']) && !empty($_GET['skill_id'])) {
    require_once 'config.php';
    setcookie('user_hash', $_GET['user_hash']);
    setcookie('skill_id', $_GET['skill_id']);
    header('Location: https://oauth.vk.com/authorize?client_id='.VK_ID.'&redirect_uri='.REDIRECT_URL.'&display=mobile');
}