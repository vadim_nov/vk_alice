<?php

if (!empty($_GET['user_hash']) && !empty($_GET['skill_id'])) {
    $config = require_once 'config.php';
    $conn = new mysqli($config['db']['host'], $config['db']['user'], $config['db']['password'], $config['db']['database']);

    if ($conn->connect_error)
        die('<a href="https://alice.ya.ru/s/'.$_GET['skill_id'].'"><h1>Назад в навык</h1></a><br>Unable to connect to the database.');

    if ($stmt = $conn->prepare('SELECT * FROM `user` WHERE `user_hash` = ?')) {
        $stmt->bind_param("s", $_GET['user_hash']);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        if ($row = $result->fetch_assoc()) {
            if (null !== $row['vk_data']) {
                echo $row['vk_data'];
            } else {
                echo HOST_URL.'/authorize.php?user_hash='.$_GET['user_hash'].'&skill_id='.$_GET['skill_id'];
            }
        } else {
            $stmt = $conn->prepare('INSERT INTO `user` (user_hash, skill_id) VALUES (?, ?)');
            $stmt->bind_param("ss", $_GET['user_hash'],$_GET['skill_id']);
            $stmt->execute();
            $stmt->close();
            echo HOST_URL.'/authorize.php?user_hash='.$_GET['user_hash'].'&skill_id='.$_GET['skill_id'];
        }
    }
    $conn->close();
}
