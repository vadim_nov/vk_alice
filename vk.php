<?php

if (!empty($_COOKIE['user_hash']) && !empty($_COOKIE['skill_id'])) {
    $config = require_once 'config.php';
    if (empty($_GET['code'])) {
        die('<a href="https://alice.ya.ru/s/'.$_COOKIE['skill_id'].'"><h1>Назад в навык</h1></a><br>No code');
    }

    $tokenJson = file_get_contents('https://oauth.vk.com/access_token?client_id='.VK_ID.'&redirect_uri='.REDIRECT_URL.'&client_secret='.SECRET_KEY.'&code='.$_GET['code']);
    $token = json_decode($tokenJson, true);
    if (empty($token)) {
        die('<a href="https://alice.ya.ru/s/'.$_COOKIE['skill_id'].'"><h1>Назад в навык</h1></a><br>No token');
    }

    $responseJson = file_get_contents('https://api.vk.com/method/users.get?user_id='.$token['user_id'].'&access_token='.$token['access_token'].'&v=5.52&fields=photo_id,verified,sex,bdate,city,country,home_town,has_photo,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max,photo_max_orig,online,domain,has_mobile,contacts,site,education,universities,schools,status,last_seen,followers_count,common_count,occupation,nickname,relatives,relation,personal,connections,exports,activities,interests,music,movies,tv,books,games,about,quotes,can_post,can_see_all_posts,can_see_audio,can_write_private_message,can_send_friend_request,is_favorite,is_hidden_from_feed,timezone,screen_name,maiden_name,crop_photo,is_friend,friend_status,career,military,blacklisted,blacklisted_by_me');
    $response = json_decode($responseJson, true);
    if (!empty($response['response'][0])) {
        $vkData = json_encode($response['response'][0],JSON_UNESCAPED_UNICODE);

        $conn = new mysqli($config['db']['host'], $config['db']['user'], $config['db']['password'], $config['db']['database']);
        if ($conn->connect_error)
            die('<a href="https://alice.ya.ru/s/'.$_COOKIE['skill_id'].'"><h1>Назад в навык</h1></a><br>Unable to connect to the database.');

        if ($stmt = $conn->prepare('UPDATE `user` SET `vk_data`  = ? WHERE `user_hash` = ?')) {
            $stmt->bind_param("ss", $vkData, $_COOKIE['user_hash']);
            if($stmt->execute()) {
                header('Location: https://alice.ya.ru/s/'.$_COOKIE['skill_id']);
            }
            $stmt->close();
        }
        $conn->close();
    }
}
